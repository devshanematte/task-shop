import React, {
	useMemo
} from 'react';
import {
	View,
	Text,
	Image,
	TouchableOpacity
} from 'react-native';
import { 
	useSelector,
	useDispatch
} from 'react-redux';
import styles from './styles';

const DefaultHeader = ({
	navigation,
	title = 'Категории'
}) => {

	const dispatch = useDispatch();
	let user = useSelector((state)=>{
		return state.user
	});

	let getRoutes = useSelector((state)=>{
		return state.routes
	});

	const goRoute = (route) => {
		dispatch({
			type:'UPDATE_CURRENT_NAVIGATION',
			route:route
		})
		return navigation.navigate(route);
	}

	return useMemo(()=>(
		<View style={styles.headerView}>
			<View style={styles.headerViewLeft}>
				<Text>Header</Text>
			</View>
			<View style={styles.headerViewRight}>
				<Text>{ title }</Text>
			</View>
		</View>
	));

}

export default DefaultHeader;