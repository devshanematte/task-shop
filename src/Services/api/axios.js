import axios from 'axios';
import config from '../../Config';

const updateAxiosConfig = async (token) => {

	axios.defaults.baseURL = config.api;
	axios.defaults.headers.common['authorization'] = token ? token : '';

}

export {
	updateAxiosConfig
}

export default axios;