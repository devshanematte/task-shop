import axios from '../axios';
import { ToastAndroid } from 'react-native';

export default {
	getCategories: () => async dispatch => {

		dispatch({
			type:'REQUEST_STATUS_CATEGORIES',
			status:true
		});

		try{

			let { data } = await axios.get('/task-shop/categories');

			dispatch({
				type:'UPDATE_CATEGORIES',
				data:data
			});

			dispatch({
				type:'REQUEST_STATUS_CATEGORIES',
				status:false
			});

			return

		}catch(err){

			dispatch({
				type:'REQUEST_STATUS_CATEGORIES',
				status:false
			});

		    ToastAndroid.showWithGravityAndOffset('Упс ошибка. Попробуйте позже',
		      	ToastAndroid.LONG,
		      	ToastAndroid.BOTTOM,
		      	0,
		      	30
		    );

			return	

		}

	},
	clearProducts: () => dispatch => {
		dispatch({
			type:'UPDATE_PRODUCTS',
			data:null
		});
		return
	},
	getProducts: (id, status) => async dispatch => {

		dispatch({
			type:'REQUEST_STATUS_PRODUCTS',
			status:true
		});

		if(status){
			dispatch({
				type:'CLEAR_PRODUCTS'
			});
		}

		try{

			let { data } = await axios.get(`/task-shop/products/${id}`);

			dispatch({
				type:'UPDATE_PRODUCTS',
				data:data
			});

			dispatch({
				type:'REQUEST_STATUS_PRODUCTS',
				status:false
			});

			return

		}catch(err){

			dispatch({
				type:'REQUEST_STATUS_PRODUCTS',
				status:false
			});

		    ToastAndroid.showWithGravityAndOffset('Упс ошибка. Попробуйте позже',
		      	ToastAndroid.LONG,
		      	ToastAndroid.BOTTOM,
		      	0,
		      	30
		    );

			return	

		}

	}
}