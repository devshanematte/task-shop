import AsyncStorage from '@react-native-community/async-storage';
import { updateAxiosConfig } from './axios';
import products from './products';
import basket from './basket';

export default {
	getToken: async () => {
		const token = await AsyncStorage.getItem('Token')
		return token
	},
	setToken: async (token) => {
		const setToken = await AsyncStorage.setItem('Token', token)
		return setToken
	},
	removeToken: async () => {
		const removeToken = await AsyncStorage.removeItem('Token')
		return removeToken
	},
	updateAxiosConfig,
	products,
	basket
}