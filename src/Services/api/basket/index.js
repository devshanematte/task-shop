import axios from '../axios';
import { ToastAndroid } from 'react-native';

export default {
	add: (item, count = 1) => async dispatch => {

		count = Number(count);

		if(count && typeof count == 'number'){

			dispatch({
				type:'ADD_ITEM_TO_BASKET',
				item,
				count
			});

			ToastAndroid.showWithGravityAndOffset('Товар в корзине',
		      	ToastAndroid.LONG,
		      	ToastAndroid.BOTTOM,
		      	0,
		      	30
		    );

		    return

		}

		ToastAndroid.showWithGravityAndOffset('Информация не валидна',
	      	ToastAndroid.LONG,
	      	ToastAndroid.BOTTOM,
	      	0,
	      	30
	    );

	    return

	}
}