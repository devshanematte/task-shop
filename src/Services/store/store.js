import { applyMiddleware, compose, createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import logger from 'redux-logger';
import ReduxThunk from 'redux-thunk';

export default (reducer) => {
  const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    blacklist: ['products']
  }

  const middleware = [];
  const enhancers = [];

  middleware.push(logger);
  middleware.push(ReduxThunk);

  const pReducer = persistReducer(persistConfig, reducer);

  enhancers.push(applyMiddleware(...middleware));

  const store = createStore(pReducer, compose(...enhancers));
  const persistor = persistStore(store);

  return {
    store, 
    persistor
  }

}
