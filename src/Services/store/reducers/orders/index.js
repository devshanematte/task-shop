import { ToastAndroid } from 'react-native';
import moment from 'moment';

const initialState = {
	list:null
}

const orders = (state = initialState, action) => {
	switch(action.type){

		case 'CREATE_ORDER' :

			ToastAndroid.showWithGravityAndOffset('Заказ успешно создан',
		      	ToastAndroid.LONG,
		      	ToastAndroid.BOTTOM,
		      	0,
		      	30
		    );

			return {
				...state,
				list:state.list && state.list.length ? [...state.list, {
					basket:action.basket,
					totalPrice:action.totalPrice,
					createdAt:moment()
				}] : [{
					basket:action.basket,
					totalPrice:action.totalPrice,
					createdAt:moment()
				}]
			}

		default :
			return state
	}
}

export default orders;