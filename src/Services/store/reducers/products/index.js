const initialState = {
	list:null,
	categories:null,

	requestStatusProducts:false,
	requestStatusCategories:false
}

const products = (state = initialState, action) => {
	switch(action.type){
		case 'REQUEST_STATUS_CATEGORIES' :
			return {
				...state,
				requestStatusCategories:action.status
			}
		case 'REQUEST_STATUS_PRODUCTS' :
			return {
				...state,
				requestStatusProducts:action.status
			}
		case 'UPDATE_CATEGORIES' :
			return {
				...state,
				categories:action.data
			}
		case 'UPDATE_PRODUCTS' :
			return {
				...state,
				list: state.list && state.list.length ? [...state.list, action.data[0]] : action.data
			}
		case 'CLEAR_PRODUCTS' :
			return {
				...state,
				list: null
			}
		default :
			return state
	}
}

export default products;