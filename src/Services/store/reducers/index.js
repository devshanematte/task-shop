import { combineReducers } from 'redux';

import products from './products';
import basket from './basket';
import orders from './orders';

export default combineReducers({
	products,
	basket,
	orders
});
