import { ToastAndroid } from 'react-native';
import _ from 'underscore';

const initialState = {
	items:null
}

const basket = (state = initialState, action) => {
	switch(action.type){

		case 'ADD_ITEM_TO_BASKET' :
			return {
				...state,
				items:state.items && state.items.length ? [...state.items, {
					...action.item,
					count:action.count
				}] : [{
					...action.item,
					count:action.count
				}]
			}

		case 'CLEAR_BASKET' :
			return {
				...state,
				items:null
			}

		case 'DELETE_ITEM_BASKET' :
			return {
				...state,
				items:_.filter(state.items, (item, index)=>{
					if(index != action.index){
						return item;
					}
				})
			}

		case 'CHANGE_COUNT_BASKET' :

			let count = Number(action.count);

			if(count && typeof count == 'number'){

				return {
					...state,
					items: state.items.map((item, index)=>{
						if(index == action.index){
							return {
								...item,
								count:action.count
							}
						}

						return item;
					})
				}

			}

			ToastAndroid.showWithGravityAndOffset('Информация не валидна',
		      	ToastAndroid.LONG,
		      	ToastAndroid.BOTTOM,
		      	0,
		      	30
		    );

			return {
				...state
			}

		default :
			return state
	}
}

export default basket;