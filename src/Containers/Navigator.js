// eslint-disable-next-line no-unused-vars
import React from 'react';
import { 
  createSwitchNavigator, 
  createAppContainer 
} from 'react-navigation';
import LaunchScreen from './Launch';
import UserNavigatorStack from './User/Navigator';

const AppNavigator = createAppContainer(
  createSwitchNavigator(
    {
      launch: { screen: LaunchScreen },
      user: { screen: UserNavigatorStack }
    },
    {
      initialRouteName: 'launch',
    },
  ),
);

export default AppNavigator;
