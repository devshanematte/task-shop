import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavigationService } from '../Services';
import AppNavigator from './Navigator';

const Context = () => {
    return (
      	<AppNavigator
        	ref={(navigatorRef) => {
          		NavigationService.setTopLevelNavigator(navigatorRef);
        	}}
      	/>
    );
}

const mapStateToProps = (state) => ({

});

export default connect(mapStateToProps)(Context);
