import { StyleSheet } from 'react-native';
import { theme } from '../../Services';

const styles = StyleSheet.create({
	view:{
		flex:1,
		justifyContent:'center',
		alignItems:'center',
		backgroundColor:'#fff',
		position:'relative'
	},
	appNameTitle:{
		textTransform:'uppercase',
		color:'#fff',
		fontSize:32,
		textShadowColor: 'rgba(0, 0, 0, 0.45)',
  		textShadowOffset: {width: -1, height: 3},
  		textShadowRadius: 7
	},
	logoApp:{
		width:'25%',
		height:60,
		resizeMode:'contain'
	},
	titleCopyright:{
		position:'absolute',
		bottom:20,
		color:'rgba(0,0,0,0.47)',
		fontFamily:theme.fonts.GilroyBold
	}
});

export default styles;

