import React, {
	useEffect
} from 'react';
import {
  View,
  Text
} from 'react-native';
import { api } from '../../Services';
import styles from './styles';

const LaunchScreen = ({
  navigation
}) => {


	const authInit = async () => {
		//Логика обработки, например если требуется обязательная авторизация пользователя, кидаем на скрин авторизации
		api.updateAxiosConfig();
		return navigation.navigate('user');
		
	}

	useEffect(()=>{
		setTimeout(()=>{
			return authInit();
		}, 1000)

		return

	}, []);

  	return (
	    <View style={styles.view}>
	      	<Text>Загрузка</Text>
	    </View>
  	)
};

export default LaunchScreen;
