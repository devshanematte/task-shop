import React, {
	useMemo,
	useState
} from 'react';
import { 
	useDispatch,
	useSelector
} from 'react-redux';
import {
	View,
	Text,
	ScrollView,
	TouchableOpacity,
	TextInput
} from 'react-native';
import styles from './styles';
import { api, utls } from '../../../Services';
import { 
	DefaultHeader
} from '../../../Components';
import Item from './Helpers/item';

const Orders = ({
	navigation,
}) => {

	let dispatch = useDispatch();
	let getOrderData = useSelector(state => state.orders);

	return useMemo(()=>(
		<View style={styles.view}>
			<DefaultHeader title='История заказов' navigation={navigation} />

			<View style={styles.viewContent}>

					<ScrollView>
						<View style={styles.viewContentPadding}>
							{
								getOrderData.list && getOrderData.list.length ?
									<View>
										<ScrollView>
											{
												getOrderData.list.map((item, index)=>{
													return <Item key={index} index={index} item={item} />
												})	
											}
										</ScrollView>
									</View>
								:
									<Text>История заказов пустая</Text>
							}
						</View>
					</ScrollView>

			</View>

		</View>
	), [getOrderData]);
}

export default Orders;