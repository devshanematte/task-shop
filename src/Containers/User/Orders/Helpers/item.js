import React from 'react';

import {
	View,
	Text,
	ScrollView,
	TouchableOpacity,
	TextInput
} from 'react-native';
import styles from '../styles';
import moment from 'moment';

const Item = ({
	item,
	index
}) => {

	return (
		<View style={styles.itemBasket}>
			<Text>Заказ номер #{index}</Text>
			<Text>Стоимость заказа: { item.totalPrice }$</Text>
			<Text>Дата создания: { moment(item.createdAt).format('DD.MM.YYYY в hh:mm:ss')}</Text>
		</View>
	);
}

export default Item;