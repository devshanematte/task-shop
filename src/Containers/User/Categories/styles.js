import { 
	StyleSheet, 
	Dimensions 
} from 'react-native';
import { theme } from '../../../Services';

let { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
	view:{
		flex:1,
		width:'100%',
		backgroundColor:'#f2f2f2'
	},
	viewContent:{
		flex:1,
		width:'100%'
	},
	viewContentPadding:{
		padding:15
	},
	otherScreenLink:{
		width:'100%',
		marginTop:100
	}
});

export default styles;
