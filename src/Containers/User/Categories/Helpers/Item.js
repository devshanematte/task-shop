import React from 'react';

import {
	View,
	Text,
	ScrollView,
	TouchableOpacity
} from 'react-native';
import styles from '../styles';

const Item = ({
	navigation,
	item
}) => {

	return (
		<TouchableOpacity onPress={()=>{ navigation.navigate('products', {
			id:item.id,
			name:item.name
		}) }}>
			<Text>{ item.name }</Text>
		</TouchableOpacity>
	);
}

export default Item;