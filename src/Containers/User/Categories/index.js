import React, {
	useMemo,
	useEffect,
	useState
} from 'react';
import { 
	useDispatch,
	useSelector
} from 'react-redux';
import {
	View,
	Text,
	ScrollView,
	TouchableOpacity
} from 'react-native';
import styles from './styles';
import { api, utls } from '../../../Services';
import { 
	DefaultHeader
} from '../../../Components';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Item from './Helpers/Item';

const Categories = ({
	navigation,
}) => {

	let dispatch = useDispatch();

	let getCategoriesData = useSelector(state => state.products);
	let getBacketData = useSelector(state => state.basket);
	let getOrderData = useSelector(state => state.orders);

	useEffect(()=>{

		dispatch(api.products.getCategories());

	}, []);

	return useMemo(()=>(
		<View style={styles.view}>
			<DefaultHeader title="Категории" navigation={navigation} />

			<View style={styles.viewContent}>
				<ScrollView>
					<View style={styles.viewContentPadding}>
						{
							getCategoriesData.requestStatusCategories ?
								<Text>Загружаем категории</Text>
							: getCategoriesData.categories && getCategoriesData.categories.length ?
								getCategoriesData.categories.map((item, index)=>{
									return <Item navigation={navigation} key={index} item={item} />
								})
							:
								<Text>Информации нет</Text>
						}
					</View>

					<View style={[styles.otherScreenLink, styles.viewContentPadding]}>
						<TouchableOpacity onPress={()=>{navigation.navigate('basket')}}>
							<Text>Корзина (товаров: {getBacketData.items && getBacketData.items.length ? getBacketData.items.length : '0'})</Text>
						</TouchableOpacity>
						<TouchableOpacity onPress={()=>{navigation.navigate('orders')}}>
							<Text>История заказов (Всего: { getOrderData.list && getOrderData.list.length ? getOrderData.list.length : '0' })</Text>
						</TouchableOpacity>
					</View>

				</ScrollView>

			</View>

		</View>
	), [getCategoriesData, getBacketData, getOrderData]);
}

export default Categories;