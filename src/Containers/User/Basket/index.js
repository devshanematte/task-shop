import React, {
	useMemo,
	useState
} from 'react';
import { 
	useDispatch,
	useSelector
} from 'react-redux';
import {
	View,
	Text,
	ScrollView,
	TouchableOpacity,
	TextInput
} from 'react-native';
import styles from './styles';
import { api, utls } from '../../../Services';
import { 
	DefaultHeader
} from '../../../Components';
import Item from './Helpers/item';

const Basket = ({
	navigation,
}) => {

	let dispatch = useDispatch();
	let getBasketData = useSelector(state => state.basket);

	let [name, setName] = useState('');
	let [adress, setAdress] = useState('');
	let [phone, setPhone] = useState('');

	let totalPrice = utls.totalPrice(getBasketData.items);

	return useMemo(()=>(
		<View style={styles.view}>
			<DefaultHeader title='Корзина' navigation={navigation} />

			<View style={styles.viewContent}>

					<ScrollView>
						<View style={styles.viewContentPadding}>
							{
								getBasketData.items && getBasketData.items.length ?
									<View>
										<ScrollView>
											{
												getBasketData.items.map((item, index)=>{
													return <Item key={index} index={index} item={item} />
												})	
											}
										</ScrollView>
									</View>
								:
									<Text>Корзина пустая</Text>
							}
						</View>
						<View style={styles.formView}>
							<TextInput style={styles.formViewInput} placeholder="Ваше Имя" value={name} onChangeText={(val)=>{setName(val)}}/>
							<TextInput style={styles.formViewInput} placeholder="Адрес" value={adress} onChangeText={(val)=>{setAdress(val)}}/>
							<TextInput style={styles.formViewInput} keyboardType="numeric" placeholder="Телефон" value={phone} onChangeText={(val)=>{setPhone(val)}}/>

							<Text>Общая стоимость: {totalPrice}$</Text>

							<TouchableOpacity onPress={()=>{
								dispatch({
									type:'CREATE_ORDER',
									totalPrice,
									basket:getBasketData.items
								});
								dispatch({
									type:'CLEAR_BASKET'
								});

								return navigation.goBack();
							}} style={styles.sendFormButton}>
								<Text style={styles.sendFormButtonText}>Оформить заказ</Text>
							</TouchableOpacity>
						</View>
					</ScrollView>

			</View>

		</View>
	), [getBasketData, name, adress, phone]);
}

export default Basket;