import { 
	StyleSheet, 
	Dimensions 
} from 'react-native';
import { theme } from '../../../Services';

let { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
	view:{
		flex:1,
		width:'100%',
		backgroundColor:'#f2f2f2'
	},
	viewContent:{
		flex:1,
		width:'100%'
	},
	viewContentPadding:{
		padding:15
	},
	itemStyleBasket:{
		width:'100%',
		padding:5,
		backgroundColor:'#fff',
		borderRadius:8,
		marginBottom:10
	},
	countProductView:{
		flexDirection:'row'
	},
	inputStyle:{
		borderWidth:1,
		borderColor:'#333',
		borderRadius:10,
		width:40,
		height:20,
		paddingTop:0,
		paddingBottom:0,
		marginLeft:10,
		color:'#444',
		paddingLeft:5,
		paddingRight:5,
		fontSize:10,
		textAlign:'center'
	},
	itemBasket:{
		width:'100%',
		marginBottom:10,
		backgroundColor:'#fff',
		padding:10,
		borderRadius:10
	},
	buttonDelete:{
		padding:5,
		backgroundColor:'red',
		borderRadius:10,
		width:100,
		flexDirection:'row',
		justifyContent:'center',
		marginTop:10,
		marginBottom:10
	},
	buttonDeleteText:{
		color:'#fff',
		fontSize:11
	},
	formView:{
		padding:15,
		width:'100%'
	},
	formViewInput:{
		width:'100%',
		height:45,
		backgroundColor:'#fff',
		marginBottom:5,
		borderRadius:7
	},
	sendFormButton:{
		width:'100%',
		height:45,
		justifyContent:'center',
		alignItems:'center',
		backgroundColor:'#5cc51a',
		borderRadius:7
	},
	sendFormButtonText:{
		color:'#fff'
	}
});

export default styles;
