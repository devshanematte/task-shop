import React, {
	useState
} from 'react';
import { 
	useDispatch
} from 'react-redux';
import {
	View,
	Text,
	ScrollView,
	TouchableOpacity,
	TextInput
} from 'react-native';
import styles from '../styles';
import { api } from '../../../../Services';

const Item = ({
	item,
	index
}) => {

	let [count, setCount] = useState(String(item.count));
	let dispatch = useDispatch();

	return (
		<View style={styles.itemBasket}>
			<Text>{ item.name }</Text>
			<Text>price: { item.price }</Text>
			<View style={styles.countProductView}>
				<Text>количество: </Text>
				<TextInput style={styles.inputStyle} placeholder="count" keyboardType="numeric" onChangeText={(val)=>setCount(val)} onSubmitEditing={()=>{
					dispatch({
						type:'CHANGE_COUNT_BASKET',
						index, 
						count
					})
				}} value={count}/>
			</View>
			<TouchableOpacity onPress={()=>{
				dispatch({
					type:'DELETE_ITEM_BASKET',
					index
				})
			}} style={styles.buttonDelete}>
				<Text style={styles.buttonDeleteText}>Удалить</Text>
			</TouchableOpacity>
		</View>
	);
}

export default Item;