import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';

import Categories from './Categories';
import Products from './Products';
import Basket from './Basket';
import Orders from './Orders';

export default createStackNavigator({
  categories:{
    screen:Categories
  },
  products:{
    screen:Products
  },
  basket:{
    screen:Basket
  },
  orders:{
    screen:Orders
  }
}, {
    navigationOptions: {
      header: null,
    },
    headerMode: 'none',
  },
);
