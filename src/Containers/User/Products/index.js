import React, {
	useMemo,
	useEffect
} from 'react';
import { 
	useDispatch,
	useSelector
} from 'react-redux';
import {
	View,
	Text,
	ScrollView,
	TouchableOpacity,
	VirtualizedList
} from 'react-native';
import styles from './styles';
import { api } from '../../../Services';
import { 
	DefaultHeader
} from '../../../Components';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Item from './Helpers/Item';
import InfiniteScroll from 'react-native-infinite-scroll';

const Products = ({
	navigation,
}) => {

	let dispatch = useDispatch();

	let getProductsData = useSelector(state => state.products);
	let { 
		id,
		name
	} = navigation.state.params;

	useEffect(()=>{

		dispatch(api.products.getProducts(id, true));

	}, []);

	return useMemo(()=>(
		<View style={styles.view}>
			<DefaultHeader title={name} navigation={navigation} />

			<View style={styles.viewContent}>

				<InfiniteScroll
			        horizontal={false}
			        onLoadMoreAsync={()=>{dispatch(api.products.getProducts(id))}}
			        distanceFromEnd={5}
			    >
					<ScrollView>
						<View style={styles.viewContentPadding}>
							{
								getProductsData.list && getProductsData.list.length ?
									getProductsData.list.map((item, index)=>{
										return <Item navigation={navigation} key={index} item={item} />
									})
								:
									<Text>Информации нет</Text>
							}
						</View>
					</ScrollView>
				</InfiniteScroll>

			</View>

		</View>
	), [getProductsData]);
}

export default Products;