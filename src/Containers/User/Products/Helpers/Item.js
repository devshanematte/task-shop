import React, {
	useState
} from 'react';

import {
	View,
	Text,
	ScrollView,
	TouchableOpacity,
	TextInput
} from 'react-native';
import styles from '../styles';
import { useDispatch } from 'react-redux';
import { api } from '../../../../Services';


const Item = ({
	navigation,
	item
}) => {

	let [count, setCount] = useState('1');
	let dispatch = useDispatch();

	return (
		<View style={styles.itemStyleBasket}>
			<Text>{ item.name }</Text>
			<Text>Price: {item.price}$</Text>
			<Text>Свойство: {item.property}</Text>
			<View style={styles.buttonBasketAdd}>
				<TouchableOpacity onPress={()=>{dispatch(api.basket.add(item, count))}}>
					<Text>В корзину</Text>
				</TouchableOpacity>
				<TextInput onChangeText={(val)=>{setCount(val)}} style={styles.inputStyle} value={count} placeholder="count" keyboardType="numeric"/>
			</View>
		</View>
	);
}

export default Item;