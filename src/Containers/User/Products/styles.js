import { 
	StyleSheet, 
	Dimensions 
} from 'react-native';
import { theme } from '../../../Services';

let { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
	view:{
		flex:1,
		width:'100%',
		backgroundColor:'#f2f2f2'
	},
	viewContent:{
		flex:1,
		width:'100%'
	},
	viewContentPadding:{
		padding:15
	},
	itemStyleBasket:{
		width:'100%',
		padding:5,
		backgroundColor:'#fff',
		borderRadius:8,
		marginBottom:10
	},
	inputStyle:{
		borderWidth:1,
		borderColor:'#333',
		borderRadius:10,
		width:40,
		height:20,
		paddingTop:0,
		paddingBottom:0,
		marginLeft:10,
		color:'#444',
		paddingLeft:5,
		paddingRight:5,
		fontSize:10,
		textAlign:'center'
	},
	buttonBasketAdd:{
		flexDirection:'row',
		marginTop:15
	}
});

export default styles;
